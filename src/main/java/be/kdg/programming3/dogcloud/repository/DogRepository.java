package be.kdg.programming3.dogcloud.repository;

import be.kdg.programming3.dogcloud.domain.Dog;

import java.util.List;

public interface DogRepository {
    List<Dog> readDogs();
    Dog createDog(Dog dog);
}
